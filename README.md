```
 __          /     __          __             /    __       \  
/ _  _._ _     _  (_ |_ _ |_  (_  _ _. _ |_  (  _ (_ |_ _ |_ ) 
\__)| |/_/_\/ _)  __)|_(_||_  __)(_| ||_)|_   \(_)__)|_(_||_/  
           /                          |        _/                 
```
# Usage   
To run `gstat`, open a command prompt window and enter the below command   
```
powershell -c "iwr https://gitlab.com/grizzyburr/gstat/-/raw/main/gstat.ps1 | iex"
```
Once the command has been run, you will have a file on your desktop titled `gstat.txt` that contains information gathered by the script