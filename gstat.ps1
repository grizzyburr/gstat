Write-Output @"
__          /     __          __             /    __       \  
/ _  _._ _     _  (_ |_ _ |_  (_  _ _. _ |_  (  _ (_ |_ _ |_ ) 
\__)| |/_/_\/ _)  __)|_(_||_  __)(_| ||_)|_   \(_)__)|_(_||_/  
           /                          |        _/              
"@
Write-Output "Gathering data..."

$cpu = get-wmiobject win32_Processor
$cpu_name = $cpu.Name
$cpu_speed = [string]([math]::round($cpu.MaxClockSpeed / 1024, 2)) + "MHz"
$cpu_output = "CPU:`n--------------------------`n" + "CPU Model: " + $cpu_name + "`n" + "CPU Base Frequency: " + $cpu_speed + "`n"

$gpu = get-ciminstance cim_videocontroller
$gpu_names = $gpu.Name -join("; ")
$gpu_version = $gpu.DriverVersion -join("; ")
$gpu_ram = $gpu.AdapterRAM -join("; ")
$gpu_output = "GPU:`n--------------------------`n" + "GPU Name(s): " + $gpu_names + "`n" + "GPU Driver(s): " + $gpu_version + "`n" + "GPU RAM Ammount(s): " + $gpu_ram /1Gb + "Gb" + "`n" + "GPU RAM amount may not be accurate if using an integrated GPU (IE, Ryzen APU or Intel graphics)" + "`n"

$memory = Get-WmiObject Win32_PhysicalMemory
$memory_size = [string]((get-wmiobject win32_physicalmemory | Measure-Object -property capacity -Sum).Sum /1Gb) + "Gb"
$memory_output = "RAM:`n--------------------------`n" + "Total Memory: " + $memory_size + "`n"

if ( (Get-Command java -ErrorAction SilentlyContinue) -eq $null ) {
    $java_ver = "Java not found"
    $java_path = "Java not found"
}
else {
    $java = get-Command java
    $java_ver = $java.Version
    $java_path = $java.Source
}
$java_output = "JAVA:`n--------------------------`nJava Version: " + $java_ver + "`n" + "Java Path: " + $java_path

$final_output = $cpu_output + "`n" + $gpu_output + "`n" + $memory_output + "`n" + $java_output
$desktop = [Environment]::GetFolderPath("Desktop")
$destination_file = $desktop +  "\gstat.txt"

Set-Content -Path $destination_file -Value $final_output
$done_prompt = "Done!...File located at " + $destination_file
Write-Output $done_prompt
Timeout /T 5
